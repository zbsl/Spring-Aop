package com.hx.advice;


import org.aspectj.lang.JoinPoint;

import java.util.Arrays;
import java.util.List;

public class LogAdvice {
    public  void beforeLog(JoinPoint joinPoint ){
        //可以通连接点获取相应的目标信息
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        List<Object> params=args == null ? null : Arrays.asList(args);
        System.out.println(methodName+"方法开始执行了----参数"+params);
    }
    protected void commit(JoinPoint joinPoint,Object result){
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        List<Object> params=args == null ? null : Arrays.asList(args);
        System.out.println(methodName+"准备提交事务了----结果:"+result+"参数:"+params);
    }
    protected void release()  {
        System.out.println("准备释放资源了----");
    }
}
