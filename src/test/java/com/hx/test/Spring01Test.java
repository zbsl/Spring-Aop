package com.hx.test;

import com.hx.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Spring01Test {
    /**
     * 事务增强功能测试
     */
    @Test
    public void aopTest(){
        ApplicationContext ioc =new ClassPathXmlApplicationContext("applicationContext-aop.xml");
        UserService userService = (UserService) ioc.getBean("userService");
        int i = userService.save(13);
        System.out.println("i="+i);

        i=userService.update(12);
        System.out.println("i="+i);

        i= userService.findAll(10);
        System.out.println("i="+i);
    }
}
